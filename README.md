This app recognises and classifies business objects from an image taken by the device. It can recognize the following classes:

   * Business Cards
   * Books
   * Cd Covers
   * Papers

The classification task is performed by a Convolutional Neural Networks (CNN), more precisely by Inception v3 network developed and trained by Google for the ImageNet Large Visual Recognition Challenge, in order to recognise up to 1000 different classes. 
Starting from this point the CNN has been retrained,using the TensorFlow library, on a self made dataset to make it able to identify the classes above.
OpenCV library is used by the app to extract rectangular shape objects in the picture, allowing it to classify multiple object at the same time. If no rectangle objects rectangular shape objects are detected, the whole image is classified.

A guide about how we retrained the inceptionV3 network can be found [here](https://www.dropbox.com/sh/twfyh2jzqs8vbl5/AAAgJ0lZe2WiIo2NOycsDQWxa?dl=0) (Italian).
The self made dataset can be found [here](https://www.dropbox.com/sh/26noqtvmxz0fret/AABuu0slRNE3r8PoNiYYM9Vza?dl=0).