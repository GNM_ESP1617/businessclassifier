package it.unipd.dei.esp1617.gnm.businessclassifier.TensorFlowClassifier;

import android.content.Context;
import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.List;

import it.unipd.dei.esp1617.gnm.businessclassifier.BusinessObject;

/**
 * Singleton class that classifies objects
 *
 * @author Nicolo, Marco, Gloria
 * @version 1.0
 */

@SuppressWarnings({"WeakerAccess", "unused"})
public class BusinessObjectClassifier {
	public static final String ACTION_CLASSIFICATION_COMPLETED = "ClassificationCompleted";

	//constants
	private static final boolean CLASSIFICATION_OK = true;
	private static final int INPUT_SIZE = 299;
	private static final int IMAGE_MEAN = 128;
	private static final float IMAGE_STD = 128;
	private static final String MODEL_FILE = "file:///android_asset/tensorflow_model_graph_5000000_iteration.pb";
	private static final String LABEL_FILE = "file:///android_asset/label_strings_500000.txt";
	private static final String INPUT_NAME = "Mul";
	private static final String OUTPUT_NAME = "final_result";

	//fields
	private static Classifier sClassifier;
	private static BusinessObjectClassifier sObjectClassifier;

	public static BusinessObjectClassifier getInstance(Context context) {
		if (sObjectClassifier == null)
			sObjectClassifier = new BusinessObjectClassifier(context);
		return sObjectClassifier;
	}


	private BusinessObjectClassifier(Context context) {
		sClassifier = TensorFlowImageClassifier.create(
				  context.getAssets(),
				  MODEL_FILE,
				  LABEL_FILE,
				  INPUT_SIZE,
				  IMAGE_MEAN,
				  IMAGE_STD,
				  INPUT_NAME,
				  OUTPUT_NAME);
	}


	/**
	 * Method that uses tensorFlow libraries and the CNN model to classify objects
	 *
	 * @param businessObject the object to classify
	 */
	public void classify(BusinessObject businessObject) {

		//retrieve the photo as a bitmap to classify...
		Bitmap bitmap = businessObject.getObjectPhoto();
		bitmap = Bitmap.createScaledBitmap(bitmap, INPUT_SIZE, INPUT_SIZE, true);

		//classify
		final List<Classifier.Recognition> results = sClassifier.recognizeImage(bitmap);

		//retrieve the classes and the precisions
		List<String> classes = new ArrayList<>(results.size());
		List<Integer> precisions = new ArrayList<>(results.size());
		for (Classifier.Recognition result: results) {
			classes.add(result.getTitle());
			precisions.add(Math.round(result.getConfidence()*100));
		}

		businessObject.setObjectClasses(classes);
		businessObject.setPrecisions(precisions);
		businessObject.setClassificationCompleted();
	}
}
