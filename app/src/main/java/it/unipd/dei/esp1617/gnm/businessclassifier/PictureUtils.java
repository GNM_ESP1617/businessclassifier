package it.unipd.dei.esp1617.gnm.businessclassifier;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;

/**
 * Utils class to scale down bitmaps
 *
 * @author Nicolo, Marco, Gloria
 * @version 1.0
 */

public class PictureUtils {
	private static Bitmap getScaledBitmap(String path, int destWidth, int destHeight) {
		//read the dim of the image on disk
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;  //the decode will return null, but the out... fields will still be set
		//so will retrieve the info needed without allocating memory for the bitmap
		BitmapFactory.decodeFile(path, options);

		int srcHeight = options.outHeight;
		int srcWidth = options.outWidth;

		int inSampleSize = 1;
		if (srcWidth > destWidth || srcHeight > destHeight) {
			int heightScale = srcHeight / destHeight;
			int widthScale = srcWidth / destWidth;

			inSampleSize = heightScale > widthScale ? heightScale : widthScale;
		}

		//apply the scaling factor
		options = new BitmapFactory.Options();
		options.inSampleSize = inSampleSize;

		return BitmapFactory.decodeFile(path, options);

	}

	public static Bitmap getScaledBitmap(String path, Activity activity) {
		Point size = new Point();
		activity.getWindowManager().getDefaultDisplay().getSize(size);

		return getScaledBitmap(path, size.x, size.y);
	}
}
