package it.unipd.dei.esp1617.gnm.businessclassifier.UI;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import java.util.List;

import it.unipd.dei.esp1617.gnm.businessclassifier.BusinessObject;
import it.unipd.dei.esp1617.gnm.businessclassifier.OpenCV.BusinessObjectListGenerator;
import it.unipd.dei.esp1617.gnm.businessclassifier.R;
import it.unipd.dei.esp1617.gnm.businessclassifier.TensorFlowClassifier.BusinessObjectClassifier;

/**
 * Activity where is displayed the taken picture.
 *
 * @author Nicolo, Marco, Gloria
 * @version 1.0 A single ImageView that displays the taken picture
 */
public class BusinessObjectPagerActivity extends AppCompatActivity {
	private static final String EXTRA_BUSINESS_OBJECT_POSITION = "it.unipd.dei.esp1617.gnm.businessclassifier.UI.business_object_position";
	private List<BusinessObject> mBusinessObjects;
	private FragmentStatePagerAdapter mFragmentPagerAdapter;
	private BroadcastReceiver mBroadcastReceiver =  new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			mFragmentPagerAdapter.notifyDataSetChanged();
		}
	};

	public static Intent newIntent(Context packageContext, int position) {
		Intent intent = new Intent(packageContext, BusinessObjectPagerActivity.class);
		intent.putExtra(EXTRA_BUSINESS_OBJECT_POSITION, position);
		return intent;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_business_object_pager);
		int position = getIntent().getIntExtra(EXTRA_BUSINESS_OBJECT_POSITION, 0);

		ViewPager viewPager = (ViewPager) findViewById(R.id.business_object_view_pager);
		mBusinessObjects = BusinessObjectListGenerator.getInstance()
				  .getRecognisedObjects();

		FragmentManager fm = getSupportFragmentManager();
		mFragmentPagerAdapter = new FragmentStatePagerAdapter(fm) {
			@Override
			public Fragment getItem(int position) {
				return BusinessObjectDetailsFragment.newInstance(position);
			}

			@Override
			public int getCount() {
				return mBusinessObjects.size();
			}

			@Override
			public int getItemPosition(Object object) {
				return POSITION_NONE;
			}
		};
		viewPager.setAdapter(mFragmentPagerAdapter);
		viewPager.setCurrentItem(position);
	}

	@Override
	protected void onResume() {
		//register the receiver
		LocalBroadcastManager.getInstance(this).registerReceiver(mBroadcastReceiver,
				  new IntentFilter(BusinessObjectClassifier.ACTION_CLASSIFICATION_COMPLETED));
		super.onResume();
	}

	@Override
	protected void onPause() {
		//unregister the receiver
		LocalBroadcastManager.getInstance(this).unregisterReceiver(mBroadcastReceiver);
		super.onPause();
	}
}

