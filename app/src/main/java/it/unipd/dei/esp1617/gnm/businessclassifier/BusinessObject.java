package it.unipd.dei.esp1617.gnm.businessclassifier;

import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Class og BusinessObjects to classify
 * @author Nicolo, Marco, Gloria
 * @version 1.0 A business object is featured by its uri (reference to bitmap) and by its class
 */

@SuppressWarnings("WeakerAccess")
public class BusinessObject {
	private static final int NUMBER_OF_CLASSES = 4;
	public final UUID id = UUID.randomUUID();

	private Bitmap mObjectPhoto;
	private List<String> mObjectClasses = new ArrayList<>(NUMBER_OF_CLASSES);
	private List<Integer> mPrecisions = new ArrayList<>(NUMBER_OF_CLASSES);
	private boolean mClassificationCompleted = false;

	/**
	 * Creates a new BusinessObject to be classified. The context is essential to know where the image is and must be saved
	 * @param objectBitmap the image representing the {@link BusinessObject}
	 */
	public BusinessObject(Bitmap objectBitmap) {
		setObjectPhoto(objectBitmap);
	}

	private void setObjectPhoto(Bitmap objectBitmap) {
		mObjectPhoto = objectBitmap;
	}

	public Bitmap getObjectPhoto() {
		return mObjectPhoto;
	}

	public String getBestObjectClass() {
		return mObjectClasses.get(0);
	}

	public int getBestClassPrecision() {
		return mPrecisions.get(0);
	}

	public void setObjectClasses(List<String> objectClasses) {
		mObjectClasses = objectClasses;
	}

	public void setPrecisions(List<Integer> precisions) {
		mPrecisions = precisions;
	}

	public List<Integer> getPrecisions() {
		return mPrecisions;
	}

	public List<String> getObjectClasses() {
		return mObjectClasses;
	}

	public boolean hasBeenClassified() {
		return mClassificationCompleted;
	}

	public void setClassificationCompleted() {
		mClassificationCompleted = true;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof BusinessObject)
			return id.equals(((BusinessObject) obj).id);
		return super.equals(obj);
	}
}