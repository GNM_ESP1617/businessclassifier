package it.unipd.dei.esp1617.gnm.businessclassifier.OpenCV;

import android.graphics.Bitmap;
import android.util.Log;

import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.CvException;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.List;

import it.unipd.dei.esp1617.gnm.businessclassifier.BusinessObject;

import static org.opencv.android.Utils.bitmapToMat;

/**
 * Singleton class that uses the openCV libraries to recognise objects and crop the image. Generates
 * a list of {@link BusinessObject} one for each sub-image.
 * @author Nicolo, Marco, Gloria
 * @version 1.0
 */

public class BusinessObjectListGenerator {
    private static BusinessObjectListGenerator sBusinessObjectListGenerator;
    private List<BusinessObject> mRecognisedObjects;
    private static final String TAG ="BusinessObjectListGen";
    private static final int ENLARGEMENT = 30;
    private static final int MIN_HEIGHT =  250;
    private static final int MIN_WIDTH =   250;

    //debug: check the correct loading of OpenCV library
    static {
        if(OpenCVLoader.initDebug())
        {
            Log.d(TAG,"OpenCV loaded successfully");
        }
        else
        {
            Log.d(TAG,"OpenCV not loaded");
        }
    }

	public static BusinessObjectListGenerator getInstance() {
		if (sBusinessObjectListGenerator == null)
			sBusinessObjectListGenerator = new BusinessObjectListGenerator();
		return sBusinessObjectListGenerator;
	}

	private BusinessObjectListGenerator() {
		mRecognisedObjects = new ArrayList<>();
	}

	/* It fills a List<BusinessObject> with the BusinessObjects found in the input image bitmap,
	  * passed as parameter. */
    public void generateObjectList(Bitmap bitmap) {
        Mat mat = getMatFromBitmap(bitmap);
        //Mat objects = mat.clone();
        Mat tmp = imagePreprocessing(mat);
        //mRecognisedObjects.add(new BusinessObject(getBitmapFromMat(tmp)));
        int n_objects = 0;

        List<MatOfPoint> contours = new ArrayList<>();
        Mat hierarchy = new Mat();

        //find the extreme outer contours in the image
        Imgproc.findContours(tmp, contours, hierarchy,
                Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);
        hierarchy.release();

        //for each contour found, check if it's rectangular-shaped
        for (int contourIdx = 0; contourIdx < contours.size(); contourIdx++) {

            MatOfPoint2f approxCurve = new MatOfPoint2f();
            MatOfPoint2f contour2f = new MatOfPoint2f(contours.get(contourIdx).toArray());

            double approxDistance = Imgproc.arcLength(contour2f, true) * 0.02;
            Imgproc.approxPolyDP(contour2f, approxCurve, approxDistance, true);

            if (approxCurve.total() == 4) {

                //find the roi that contains the object found
                Rect boundingRoi = Imgproc.boundingRect(contours.get(contourIdx));

                //to filter some false positives
                if (boundingRoi.height > MIN_HEIGHT && boundingRoi.width > MIN_WIDTH) {
                    //Imgproc.drawContours(objects, contours, contourIdx, new Scalar(255, 0, 0,
                    //.8), 8);

                    //Enlarging the region of interest is better for classification
                    Rect roi = enlargeRoi(mat, boundingRoi, ENLARGEMENT);
                    Mat cropped = mat.submat(roi);

                    //do the crop and add to the list
                    Bitmap bmp = getBitmapFromMat(cropped);
                    mRecognisedObjects.add(new BusinessObject(bmp));
                    n_objects++;
                } else {
                    Log.d(TAG, "Object non selected");
                }
            }
        }
        if (n_objects == 0) { //if the algorithm doesn't find any object, it add the original image
                Log.d(TAG, "Not found any object");
                Bitmap bmp = getBitmapFromMat(mat);
                mRecognisedObjects.add(new BusinessObject(bmp));

        } else {
                if (n_objects == 1) {
                    Log.d(TAG, "Found an object ");
                } else {
                    Log.d(TAG, "Found " + n_objects + " objects");
                }
                //Bitmap bmp = getBitmapFromMat(objects);
                //mRecognisedObjects.add(new BusinessObject(bmp));
        }
    }

    /* It converts the input image mBitmap, passing as parameter, in a Mat */
    private static Mat getMatFromBitmap(Bitmap mBitmap) {

        Mat mat = new Mat(mBitmap.getWidth(), mBitmap.getHeight(), CvType.CV_8UC4);
        try {
            Bitmap tmp = mBitmap.copy(Bitmap.Config.ARGB_8888, true);
            bitmapToMat(tmp,mat);
        }
        catch (CvException e) {
            Log.d(TAG,e.getMessage());
        }
        return mat;

    }

    /* It converts the input Mat mMat, passing as parameter, in a Bitmap */
    private static Bitmap getBitmapFromMat(Mat mMat){

        Bitmap bmp = null;

        try {
            bmp = Bitmap.createBitmap(mMat.cols(), mMat.rows(), Bitmap.Config.ARGB_8888);
            Utils.matToBitmap(mMat, bmp);
        }
        catch (CvException e) {
            Log.d(TAG,e.getMessage());
        }
        return bmp;

    }

    /* It does the preprocessing necessary to ease the search of rectangular-shaped objects.
     * It accepts as parameter mMat to manipulate. */
    private static Mat imagePreprocessing(Mat mMat)
    {
        Mat tmp = new Mat();
        //first preprocessing is the conversion of image in grayscale.
        Imgproc.cvtColor(mMat,tmp,Imgproc.COLOR_RGB2GRAY);
        org.opencv.core.Size s = new Size(7,7);

        //apply a filter to smooth the input image and to reduce noise. Our tests showed that gaussianblur works better.
        Imgproc.GaussianBlur(tmp, tmp, s, 1.5, 1.5);
        //Imgproc.bilateralFilter(tmp,tmp,9,80,80);
        //Imgproc.medianBlur(tmp, tmp, 9);

        //Apply Canny to detect edges. We use L2 norm to obtain more accurate results
        Imgproc.Canny(tmp, tmp, 10, 65, 3, true);

        //The output is a structuring element of the shape (elliptic) useful for the next operations.
        //In our tests with the flag Imgproc.MORPH_ELLIPSE instead of Imgproc.MORPH_RECT we obtained less false positives.
        Mat kernel = Imgproc.getStructuringElement(Imgproc.MORPH_ELLIPSE, new Size(2,2));
        //Mat kernel = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(2,2));

        //do a convolution between the preprocessed image and the kernel found during the previous operation.
        //This is useful to point out the white edges found by Canny detector
        Imgproc.dilate(tmp,tmp, kernel);
        return tmp;
    }

    /* It computes a region of interest in mMat, passing as parameter, enlarging the mBoundedRoi (parameter)
    according to the padding (parameter).
     */
    private Rect enlargeRoi(Mat mMat, Rect mBoundedRoi, int padding) {
        Rect roi = new Rect(mBoundedRoi.x - padding, mBoundedRoi.y - padding, mBoundedRoi.width + (padding * 2), mBoundedRoi.height + (padding * 2));
        if (roi.x < 0)roi.x = 0;
        if (roi.y < 0)roi.y = 0;
        if (roi.x+roi.width >= mMat.cols())roi.width = mMat.cols()-roi.x;
        if (roi.y+roi.height >= mMat.rows())roi.height = mMat.rows()-roi.y;
        return roi;
    }

    public List<BusinessObject> getRecognisedObjects() {
        return mRecognisedObjects;
    }

	public void resetList() {
		mRecognisedObjects = new ArrayList<>();
	}
}
