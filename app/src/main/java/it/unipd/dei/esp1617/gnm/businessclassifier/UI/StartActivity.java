package it.unipd.dei.esp1617.gnm.businessclassifier.UI;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import it.unipd.dei.esp1617.gnm.businessclassifier.R;


/**
 * Launching Activity.
 *
 * @author Nicolo, Marco, Gloria
 * @version 1.0 A single button for taking a picture to classify
 */
public class StartActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_start);

		//start button + listener
		Button startButton = (Button) findViewById(R.id.start_button);
		startButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(StartActivity.this, BusinessObjectListActivity.class);
				startActivity(intent);
			}
		});

		//info button + listener
		FloatingActionButton infoButton = (FloatingActionButton) findViewById(R.id.info_button);
		infoButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(), InfoActivity.class);
				startActivity(intent);
			}
		});
	}
}
