package it.unipd.dei.esp1617.gnm.businessclassifier.UI;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.List;

import it.unipd.dei.esp1617.gnm.businessclassifier.BusinessObject;
import it.unipd.dei.esp1617.gnm.businessclassifier.OpenCV.BusinessObjectListGenerator;
import it.unipd.dei.esp1617.gnm.businessclassifier.R;

/**
 * Fragment that displays the details of a {@link BusinessObject} that has UUID equals to the one
 * passed as parameter in the newInstance method.
 *
 * @author Nicolo, Marco, Gloria
 * @version 1.0
 */
public class BusinessObjectDetailsFragment extends Fragment {
	private static final String ARG_BUSINESS_OBJECT_POSITION = "business_object_id";
	private BusinessObject mBusinessObject;
	private BusinessObjectListGenerator mBusinessObjectListGenerator = BusinessObjectListGenerator.getInstance();

	public static BusinessObjectDetailsFragment newInstance(int businessObjectID) {
		BusinessObjectDetailsFragment fragment = new BusinessObjectDetailsFragment();
		Bundle args = new Bundle();
		args.putInt(ARG_BUSINESS_OBJECT_POSITION, businessObjectID);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getArguments() != null) {
			int position = getArguments().getInt(ARG_BUSINESS_OBJECT_POSITION);
			mBusinessObject = mBusinessObjectListGenerator.getRecognisedObjects().get(position);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_business_object_details, container, false);

		//display the image
		ImageView photoImageView = (ImageView) v.findViewById(R.id.business_object_photo);
		photoImageView.setImageBitmap(mBusinessObject.getObjectPhoto());

		//display the classes
		List<String> objectClasses = mBusinessObject.getObjectClasses();
		List<Integer> precisions = mBusinessObject.getPrecisions();
		TableLayout table = (TableLayout) v.findViewById(R.id.classes_list_table);
		for (int i = 0; i < Math.min(table.getChildCount(), objectClasses.size()); i++) {
			TableRow row = (TableRow) table.getChildAt(i);

			//set class textview
			TextView objectClass = (TextView) row.getChildAt(0);
			objectClass.setText(objectClasses.get(i));

			//set precision textview
			TextView precision = (TextView) row.getChildAt(1);
			precision.setText(getString(R.string.classification_confidence, precisions.get(i)));
		}

		return v;
	}
}
