package it.unipd.dei.esp1617.gnm.businessclassifier.UI;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import it.unipd.dei.esp1617.gnm.businessclassifier.R;

public class InfoActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_info);

		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
		fab.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent intent = new Intent(Intent.ACTION_SENDTO);
				intent.setData(Uri.parse("mailto:"));
				String[] addresses = {
						  "nicolo.soleti@studenti.unipd.it",
						  "marco.brugnera.1@studenti.unipd.it",
						  "gloria.beraldo@studenti.unipd.it"
				};
				intent.putExtra(Intent.EXTRA_EMAIL, addresses);
				if (intent.resolveActivity(getPackageManager()) != null)
					startActivity(intent);
			}
		});
	}
}


