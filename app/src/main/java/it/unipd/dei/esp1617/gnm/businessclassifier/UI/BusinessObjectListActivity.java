package it.unipd.dei.esp1617.gnm.businessclassifier.UI;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.List;

import it.unipd.dei.esp1617.gnm.businessclassifier.BusinessObject;
import it.unipd.dei.esp1617.gnm.businessclassifier.OpenCV.BusinessObjectListGenerator;
import it.unipd.dei.esp1617.gnm.businessclassifier.PictureUtils;
import it.unipd.dei.esp1617.gnm.businessclassifier.R;
import it.unipd.dei.esp1617.gnm.businessclassifier.TensorFlowClassifier.BusinessObjectClassifier;

/**
 * Activity that contains a list of {@link BusinessObject}
 * @author Nicolo, Marco, Gloria
 * @version 3.0
 */
@SuppressWarnings("FieldCanBeLocal")
public class BusinessObjectListActivity extends AppCompatActivity {
	//constants
	private static final String EXTRA_POSITION = "it.unipd.dei.esp1617.gnm.businessclassifier.UI.ExtraPosition";
	private static final String IMG_TO_CLASSIFY_FILENAME = "IMG_TO_CLASSIFY.jpg";
	private static final String FILE_PROVIDER_AUTHORITY = "it.unipd.dei.esp1617.gnm.businessclassifier.fileprovider";
	private static final String TAG = "BusinessObjectListAct";
	private static final int GOOD_CONFIDENCE_BOUND = 80;
	private static final int BAD_CONFIDENCE_BOUND = 50;
	private static final int REQUEST_PHOTO = 0;

	private File mPhotoToClassify;

	//UI fields
	private RecyclerView businessObjectRecyclerView;
	private BusinessObjectAdapter mBusinessObjectAdapter;
	private FloatingActionButton mNewPhotoFab;
	private FloatingActionButton mRemovePhotoFab;
	private FloatingActionButton mConfirmDeletion;
	private TextView mNoItemTextView;

	//fields for selection mode
	private boolean selectionMode = false;
	private boolean[] mSelectedItems;
	private int numSelectedItems = 0;
	private Snackbar mSelectionModeSnackbar;

	//processing objects
	private BusinessObjectClassifier mBusinessObjectClassifier;
	private BusinessObjectListGenerator mBusinessObjectListGenerator;
	private final BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			int position = intent.getIntExtra(EXTRA_POSITION, 0);
			mBusinessObjectAdapter.notifyItemChanged(position);
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_business_object_list);

		//instantiate the classifier and the object recognizer
		mBusinessObjectClassifier = BusinessObjectClassifier.getInstance(this);
		mBusinessObjectListGenerator = BusinessObjectListGenerator.getInstance();

		//retrieve references to UI elements
		mNewPhotoFab = (FloatingActionButton) findViewById(R.id.take_new_photo_fab);
		mRemovePhotoFab = (FloatingActionButton) findViewById(R.id.remove_object_fab);
		mConfirmDeletion = (FloatingActionButton) findViewById(R.id.confirm_deletion_button);
		mNoItemTextView = (TextView) findViewById(R.id.no_item_text_view);

		//setup recycler view
		businessObjectRecyclerView = (RecyclerView) findViewById(R.id.business_object_list);
		businessObjectRecyclerView.setLayoutManager(new LinearLayoutManager(this));
		mBusinessObjectAdapter = new BusinessObjectAdapter();
		businessObjectRecyclerView.setAdapter(mBusinessObjectAdapter);

		//setup the UI based on the current mode
		mSelectionModeSnackbar = Snackbar.make(findViewById(R.id.coordinator_layout), getString(R.string.selected_items_label, numSelectedItems), Snackbar.LENGTH_INDEFINITE)
				  .setAction(R.string.snackbar_action_button, new View.OnClickListener() {
					  @Override
					  public void onClick(View v) {
						  //back to normal mode
						  selectionMode = false;
						  updateUI();
						  mBusinessObjectAdapter.notifyDataSetChanged();
					  }
				  });

		//setup new photo listener
		mNewPhotoFab.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				mPhotoToClassify = new File(getFilesDir(), IMG_TO_CLASSIFY_FILENAME);

				//intent for taking a photo
				Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
				Uri uri = FileProvider.getUriForFile(getApplicationContext(),
						  FILE_PROVIDER_AUTHORITY,
						  mPhotoToClassify);
				cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);

				//find the list of activities that the intent can resolve to
				List<ResolveInfo> cameraActivities =
						  getPackageManager().queryIntentActivities(cameraIntent, PackageManager.MATCH_DEFAULT_ONLY);

				//grant permission to each activity to write the file pointed by the uri
				for (ResolveInfo activity : cameraActivities) {
					getApplicationContext().grantUriPermission(activity.activityInfo.packageName,
							  uri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
				}

				//start the activity
				startActivityForResult(cameraIntent, REQUEST_PHOTO);
			}
		});

		//set up remove object listener
		mRemovePhotoFab.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				//enter in selection mode
				selectionMode = true;
				updateUI();
				mBusinessObjectAdapter.notifyDataSetChanged();

				//show hints for the user
				Toast.makeText(BusinessObjectListActivity.this, R.string.select_items_to_remove_toast, Toast.LENGTH_SHORT)
						  .show();
			}
		});

		//setup confirm deletion listener
		mConfirmDeletion.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				for (int i = mSelectedItems.length -1 ; i > -1; i--) {
					boolean itemSelected = mSelectedItems[i];
					if (itemSelected) {
						//remove images from the list
						mBusinessObjectListGenerator.getRecognisedObjects().remove(i);
					}
				}

				//go back to normal mode
				selectionMode = false;
				updateUI();
				mBusinessObjectAdapter.notifyDataSetChanged();
			}
		});
	}

	@Override
	protected void onResume() {
		mBusinessObjectAdapter.notifyDataSetChanged();
		LocalBroadcastManager.getInstance(this).registerReceiver(mBroadcastReceiver,
				  new IntentFilter(BusinessObjectClassifier.ACTION_CLASSIFICATION_COMPLETED));
		super.onResume();
	}

	@Override
	protected void onPause() {
		LocalBroadcastManager.getInstance(this).unregisterReceiver(mBroadcastReceiver);
		super.onPause();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode != RESULT_OK) {
			Log.e(TAG, "Error in retrieving results");
			return;
		}

		switch (requestCode) {
			case REQUEST_PHOTO:
				//relieve the photo
				Uri uri = FileProvider.getUriForFile(getApplicationContext(),
						  FILE_PROVIDER_AUTHORITY,
						  mPhotoToClassify);
				//revoke permission to the camera
				revokeUriPermission(uri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

				//classify in a separate thread
				Bitmap bitmap = PictureUtils.getScaledBitmap(mPhotoToClassify.getPath(), BusinessObjectListActivity.this);
				new ObjectRecognitionTask().execute(bitmap);
				break;
			default:
				Log.e(TAG, "Request code " + requestCode + " not managed");
		}
	}

	@Override
	public void onBackPressed() {
		mBusinessObjectListGenerator.resetList();
		super.onBackPressed();
	}

	private void updateUI() {
		//display "no item" and remove the remove button if the list is empty
		if (mBusinessObjectListGenerator.getRecognisedObjects().size()==0) {
			mRemovePhotoFab.setVisibility(View.INVISIBLE);
			mNoItemTextView.setVisibility(View.VISIBLE);
		} else {
			mRemovePhotoFab.setVisibility(View.VISIBLE);
			mNoItemTextView.setVisibility(View.INVISIBLE);
		}

		if (selectionMode) {
			//array to know selected items
			mSelectedItems = new boolean[mBusinessObjectListGenerator.getRecognisedObjects().size()];

			//update UI
			mNewPhotoFab.setVisibility(View.INVISIBLE);
			mRemovePhotoFab.setVisibility(View.INVISIBLE);
			mConfirmDeletion.setVisibility(View.VISIBLE);
			mSelectionModeSnackbar.show();
		} else {
			mSelectedItems = null;
			numSelectedItems = 0;

			mConfirmDeletion.setVisibility(View.INVISIBLE);
			mNewPhotoFab.setVisibility(View.VISIBLE);
			mSelectionModeSnackbar.dismiss();
			mSelectionModeSnackbar.setText(getString(R.string.selected_items_label, numSelectedItems)); //reset the snackbar
		}
	}

	@SuppressWarnings("WeakerAccess")
	private class BusinessObjectAdapter extends RecyclerView.Adapter<BusinessObjectHolder> {

		@Override
		public BusinessObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
			LayoutInflater inflater = LayoutInflater.from(parent.getContext());
			return new BusinessObjectHolder(inflater, parent);
		}

		@Override
		public void onBindViewHolder(final BusinessObjectHolder holder, int position) {
			holder.setCurrentListener();
			holder.bind(mBusinessObjectListGenerator.getRecognisedObjects().get(position));
		}

		@Override
		public int getItemCount() {
			return mBusinessObjectListGenerator.getRecognisedObjects().size();
		}
	}

	@SuppressWarnings("WeakerAccess")
	private class BusinessObjectHolder extends RecyclerView.ViewHolder {
		//Model objects
		private BusinessObject mBusinessObject;

		//UI objects
		private ImageView mPhotoImageView;
		private TextView mClassTextView;
		private TextView mPercentageTextView;
		private ProgressBar mClassificationProgressBar;
		private TextView mProgressTextView;

		public BusinessObjectHolder(LayoutInflater inflater, ViewGroup parent) {
			super(inflater.inflate(R.layout.list_item_business_object, parent, false));

			//ref to UI items
			mPhotoImageView = (ImageView) itemView.findViewById(R.id.business_object_photo);
			mClassTextView = (TextView) itemView.findViewById(R.id.business_object_class);
			mPercentageTextView = (TextView) itemView.findViewById(R.id.percentage_text_view);
			mClassificationProgressBar = (ProgressBar) itemView.findViewById(R.id.classification_progress_bar);
			mProgressTextView = (TextView) itemView.findViewById(R.id.classifying_progress_message);
		}

		public void setCurrentListener() {
			//if normal mode, clicking on an item should display the object details...
			if (!selectionMode) {
				itemView.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						Intent intent = BusinessObjectPagerActivity.newIntent(BusinessObjectListActivity.this, getAdapterPosition());
						startActivity(intent);
					}
				});
			}
			//if selection mode, clicking on an item changes the background color...
			else {
				itemView.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						mSelectedItems[getAdapterPosition()] = !mSelectedItems[getAdapterPosition()];
						if (mSelectedItems[getAdapterPosition()]) {
							itemView.setBackgroundColor(ContextCompat.getColor(BusinessObjectListActivity.this, R.color.selected_item_background));
							numSelectedItems++;
						}
						else {
							itemView.setBackgroundColor(Color.TRANSPARENT);
							numSelectedItems--;
						}
						mSelectionModeSnackbar.setText(getString(R.string.selected_items_label, numSelectedItems)).show();
					}
				});
			}
		}

		public void bind(BusinessObject businessObject) {
			//display the bitmap
			mBusinessObject = businessObject;
			mPhotoImageView.setImageBitmap(mBusinessObject.getObjectPhoto());

			//if classified... display the image
			if (mBusinessObject.hasBeenClassified()) {
				//delete the progress bar from UI
				mClassTextView.setVisibility(View.VISIBLE);
				mPercentageTextView.setVisibility(View.VISIBLE);
				mClassificationProgressBar.setVisibility(View.INVISIBLE);
				mProgressTextView.setVisibility(View.INVISIBLE);

				//display the class
				mClassTextView.setText(mBusinessObject.getBestObjectClass());

				//display the percentage with the color
				int percentage = mBusinessObject.getBestClassPrecision();
				mPercentageTextView.setText(getResources()
						  .getString(R.string.classification_confidence, percentage));
				int color;
				if (percentage >= GOOD_CONFIDENCE_BOUND)
					color = ContextCompat.getColor(BusinessObjectListActivity.this, R.color.good_confidence);
				else if (percentage < BAD_CONFIDENCE_BOUND)
					color = ContextCompat.getColor(BusinessObjectListActivity.this, R.color.bad_confidence);
				else
					color = ContextCompat.getColor(BusinessObjectListActivity.this, R.color.medium_confidence);
				mPercentageTextView.setTextColor(color);
			} else {
				mClassTextView.setVisibility(View.INVISIBLE);
				mPercentageTextView.setVisibility(View.INVISIBLE);
				mClassificationProgressBar.setVisibility(View.VISIBLE);
				mProgressTextView.setVisibility(View.VISIBLE);
			}

			//display the correct background
			if (!selectionMode){
				itemView.setBackgroundColor(Color.TRANSPARENT);
			} else {
				if (mSelectedItems[getAdapterPosition()])
					itemView.setBackgroundColor(ContextCompat.getColor(BusinessObjectListActivity.this, R.color.selected_item_background));
				else
					itemView.setBackgroundColor(Color.TRANSPARENT);
			}

		}
	}

	private class ObjectRecognitionTask extends AsyncTask<Bitmap, Void, Void> {
		Snackbar generationInProgressSnackbar = Snackbar
				  .make(findViewById(R.id.coordinator_layout), R.string.snackbar_openCV_message, Snackbar.LENGTH_INDEFINITE);

		@Override
		protected void onPreExecute() {
			generationInProgressSnackbar.show();
		}

		@Override
		protected Void doInBackground(Bitmap... params) {
			mBusinessObjectListGenerator.generateObjectList(params[0]);
			return null;
		}

		@Override
		protected void onPostExecute(Void aVoid) {
			//update UI
			updateUI();
			generationInProgressSnackbar.dismiss();
			mBusinessObjectAdapter.notifyDataSetChanged();

			//call the classification task
			List<BusinessObject> objects = mBusinessObjectListGenerator.getRecognisedObjects();
			BusinessObject[] businessObjects = new BusinessObject[objects.size()];
			for (int i = 0; i < businessObjects.length; i++) {
				businessObjects[i] = objects.get(i);
			}

			new ClassifyTask().execute(businessObjects);
		}
	}

	private class ClassifyTask extends AsyncTask<BusinessObject, Integer, Void> {
		@Override
		protected Void doInBackground(BusinessObject... params) {
			for (int i = 0; i < params.length; i++) {
				BusinessObject businessObject = params[i];
				if (!businessObject.hasBeenClassified()) {
					mBusinessObjectClassifier.classify(businessObject);
					publishProgress(i); //update the i-th element
				}
			}
			return null;
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			Intent broadcastIntent = new Intent(BusinessObjectClassifier.ACTION_CLASSIFICATION_COMPLETED)
					  .putExtra(EXTRA_POSITION, values[0]);
			LocalBroadcastManager.getInstance(getApplicationContext())
					  .sendBroadcast(broadcastIntent);
		}
	}
}